import sys
import math
import numpy

def angular_dist(vec1, vec2):
  sim = cosine_sim(vec1, vec2)
  try:
    return 1 - math.acos(sim) / math.pi
  except ValueError:
    if abs(sim - 1.0) < sys.float_info.epsilon * max(abs(sim), 1.0):
      return 1.0
    print(cosine_sim(vec1, vec2))

def cosine_sim(vec1, vec2):
  v1 = numpy.array(vec1)
  v2 = numpy.array(vec2)

  try:
    return float(numpy.dot(v1, v2) / (numpy.linalg.norm(v1) * numpy.linalg.norm(v2)))
  except ZeroDivisionError:
    return 0.0

def jaccard_sim(vec1, vec2):
  set1 = vec1
  set2 = vec2
  if type(vec1) is not set:
    set1 = set(vec1)
  if type(vec2) is not set:
    set2 = set(vec2)
  intersection = set1 & set2
  union = set1 | set2

  try:
    return float(len(intersection) / len(union))
  except ZeroDivisionError:
    return 0.0

def dice_sim(vec1, vec2):
  set1 = set(vec1)
  set2 = set(vec2)
  intersection = set1 & set2

  try:
    return float(2 * len(intersection) / (len(vec1) + len(vec2)))
  except ZeroDivisionError:
    return 0.0

def simpson_sim(vec1, vec2):
  set1 = set(vec1)
  set2 = set(vec2)
  intersection = set1 & set2

  try:
    return float(len(intersection) / min([len(vec1), len(vec2)]))
  except ZeroDivisionError:
    return 0.0

def euclidean_dist(vec1, vec2):
  v1 = numpy.array(vec1)
  v2 = numpy.array(vec2)
  return float(numpy.linalg.norm(v1 - v2, ord=2))

def manhattan_dist(vec1, vec2):
  v1 = numpy.array(vec1)
  v2 = numpy.array(vec2)
  return float(numpy.linalg.norm(v1 - v2, ord=1))
