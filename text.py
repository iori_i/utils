import re
import MeCab

OPEN_BRACKETS = ['「', '〚', '(', '（', '【', '『', '［', '〈', '《', '〔', '｛', '{', '｛', '«', '‹', '〘', '〚', '「', '[', '［', '｢']
CLOSE_BRACKETS = ['」', '〛', ')', '）', '】', '』', '］', '〉', '》', '〕', '｝', '}', '｝', '»', '›', '〙', '〛', '」', ']', '］', '｣']
QUOTES = ['†', '‡']
SPLITTERS = ['。', '？', '?', '!', '！','…']
MAX_LENGTH = 1024

# Judge s is substring of t or not
def is_substring(s, t):
  len_s = len(s)
  len_t = len(t)
  if len_s == 0: return True
  if len_t == 0: return False

  i = 0 # probe of t
  for c in s:
    while i < len_t and c != t[i]: i += 1
    if i >= len_t: return False
    i += 1

  return True

def is_contained(parent, child):
  parent_set = set(parent)
  child_set  = set(child)
  union_set  = parent_set | child_set

#def is_skipsubstring(sentence1, sentence2, separater=' '):
def is_subsequence(sequence1, sequence2, separater=' '):
  if sequence1 == sequence2:
    return True

  if type(sequence1) is str:
    sequence1 = sequence1.split(separater)

  if type(sequence2) is str:
    sequence2 = sequence2.split(separater)

  case1 = is_contained(sequence1, sequence2)
  case2 = is_contained(sequence2, sequence1)

  if not case1 and not case2:
    return False

  if case1:
    pos = -1
    skipsubstring = True
    for w in sequence2:
      try:
        idx = sequence1.index(w, pos + 1)
        if idx < pos:
          skipsubstring = False
          break
      except ValueError:
        break
    if skipsubstring:
      return True

  if case2:
    pos = -1
    skipsubstring = True
    for w in sequence1:
      try:
        idx = sequence2.index(w, pos + 1)
        if idx < pos:
          skipsubstring = False
          break
      except ValueError:
        break
    if skipsubstring:
      return True

  return False

ZEN = "".join(chr(0xff01 + i) for i in range(94))
HAN = "".join(chr(0x21 + i) for i in range(94))
ZEN2HAN = str.maketrans(ZEN, HAN)
CONTROLES = str.maketrans({'\t': ' ', '\v': '', '\u0010': '', '\u0011': '', '\u0012': '', '\u0013': '', '\u0014': '', '\u0015': '', '\u0016': '', '\u0017': '', '\u0018': '', '\u0019': '', '\u001a': '', '\u001b': '', '\u001c': '', '\u001d': '', '\u001e': '', '\u001f': ''})
def normalize(text, double_space=False):
  text = text.replace("&quot;", "\"")
  text = text.translate(CONTROLES)
  if double_space:
    text = text.replace("  ", " ")

  # Zenkaku -> Hankaku
  text = text.translate(ZEN2HAN)

  # ToDo: Parenthesis
  # ToDo: White Parenthesis
  # ToDo: Corner Bracket
  # ToDo: White Corner Bracket
  # ToDo: Square Bracket
  # ToDo: Curly Bracket
  # ToDo: Tortoise Shell Bracket
  # ToDo: Angle Bracket
  # ToDo: Double Angle Bracket
  # ToDo: Black Lenticular Bracket
  return text.strip()

def separate(text):
  lines = text.replace("\r", "").split("\n")

  valid_lines = []
  invalid_lines = []

  for line in lines:
    quote = 0
    bracket = 0
    sentence = ""
    for idx, ch in enumerate(text):
      sentence += ch
      if ch in OPEN_BRACKETS:
        bracket += 1
      elif ch in CLOSE_BRACKETS:
        bracket -= 1
      elif ch in QUOTES:
        quote += 1
      elif ch in SPLITTERS:
        if bracket != 0 or quote % 2 == 1:
          continue
        if len(text) != (idx + 1) and text[idx + 1] in SPLITTERS:
          continue
        if len(sentence) >= MAX_LENGTH:
          invalid_lines.append(sentence.strip())
        else:
          valid_lines.append(sentence.strip())
        sentence = ""
    if bracket != 0 or sentence != "" or quote % 2 != 1:
      invalid_lines.append(sentence)

  return valid_lines, invalid_lines

class Wakachigaki():
  def __init__(self):
    self.article_cnt = 0
    self.article_date = set()
    self.sentence_cnt = 0
    self.sentence_len = 0
    self.token_cnt = 0
    self.content_token_cnt = 0
    self.uniq_token = set()
    self.uniq_content_token = set()

    #self.mecab = MeCab.Tagger('-d /var/lib/mecab/dic/debian/sys.dic')
    #self.mecab = MeCab.Tagger('-d /var/lib/x86_64-linux-gnu/mecab/dic/mecab-ipadic-neologd')
    self.mecab = MeCab.Tagger('-d /var/lib/mecab/dic/mecab-ipadic-neologd')
    self.mecab.parse('')

  def wakachi(self, text):
    node = self.mecab.parseToNode(text)
    original_words = []
    content_words = []
    while node:
      part = node.feature.split(",")[0]
      original_words.append(node.surface)
      if part in ["名詞", "形容詞", "動詞", "副詞"]:
        content_words.append(node.surface)
        self.content_token_cnt += 1
        self.uniq_content_token.add(node.surface)

      self.token_cnt += 1
      self.uniq_token.add(node.surface)
      node = node.next

    return original_words, content_words

  def wakachi_article(self, article):
    self.article_cnt += 1
    self.article_date.add(article['DATE'] // 100000)

    wakachi_body = []
    for sentence in article['BODY']:
      original, content = self.wakachi(sentence)
      if content and len(content) != 0:
        self.sentence_cnt += 1
        self.sentence_len += len(sentence)
        wakachi_body.append({
          'ORIGINAL': sentence,
          'WAKATI': content, # Fix: WAKATI => WAKACHI
        })
    article['BODY'] = wakachi_body
    return article

