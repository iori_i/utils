def ask_yes_no():
  dic = {
    'y': True, 'yes': True,
    'n': False, 'no': False
  }
  while True:
    ask = input('y/n >> ').lower()
    if ask in dic:
      return dic[ask]
