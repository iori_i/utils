from bert_serving.client import BertClient
import sentencepiece as spm

from utils.text import normalize

SENTENCE_PIECE_DIC = '../data/bert/wiki-ja.model'

class Bert():
  def __init__(self, host='dev-bert', timeout=3600000):
    self.client = BertClient(ip=host, timeout=timeout)
    s = spm.SentencePieceProcessor()
    s.Load(SENTENCE_PIECE_DIC)
    self.s = s

  def encode(self, words_list):
    if len(words_list) == 0:
      return None

    return self.client.encode(words_list, is_tokenized=True)

  def parse(self, texts):
    if len(texts) == 0:
      return None

    if type(texts) is str:
      texts = [texts]

    parsed_sentences = [self._parse(normalize(s)) for s in texts]
    return list(filter(lambda x: len(x) > 0, parsed_sentences))

  def _parse(self, text):
    return self.s.EncodeAsPieces(text.lower())


