import os
import json
try:
  import _pickle as pickle
except:
  import pickle

from utils.numpy_encoder import NumpyEncoder

def create_dir(path):
  if not os.path.isdir(path):
    os.makedirs(path)
  return path

def new_filename(dirname, path, prefix, postfix):
  filename, ext = os.path.splitext(os.path.basename(path))
  return os.path.join(dirname, prefix + filename + postfix)

def save_json(path, data, is_indent=False):
  dirname = os.path.dirname(path)
  if dirname != '':
    create_dir(dirname)

  with open(path, 'w') as fw:
    if is_indent:
      json.dump(data, fw, ensure_ascii=False, indent=2, cls=NumpyEncoder)
    else:
      json.dump(data, fw, ensure_ascii=False, cls=NumpyEncoder)

def load_json(path):
  with open(path, 'r') as f:
    return json.load(f)

def save_pickle(path, data):
  dirname = os.path.dirname(path)
  if dirname != '':
    create_dir(dirname)

  with open(path, 'wb') as fw:
    pickle.dump(data, fw)

def load_pickle(path):
  with open(path, 'rb') as f:
    return pickle.load(f)
